## [1.0.4] - 18 April 2020

* Add displaying library's log call site

## [1.0.3] - 18 April 2020

* Handle error when log called from library and package path not found

## [1.0.2] - 16.04.2020

* Make package independent from flutter


## [1.0.1] - 16.04.2020

* Rename file according convention


## [1.0.0] - 16.04.2020

* Add existing sources