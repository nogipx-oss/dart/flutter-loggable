import 'package:logger/logger.dart';


mixin Loggable {

  Logger get log {
    final filter = DebugFilter();
    filter.level = Level.wtf;
    return Logger(
      filter: filter,
      printer: SimpleLogPrinter(
        className: this.runtimeType.toString(),
      )
    );
  }
}


class SimpleLogPrinter extends LogPrinter {
  final String className;
  SimpleLogPrinter({this.className});

  static final levelColors = {
    Level.verbose: 008,
    Level.debug: 007,
    Level.info: 012,
    Level.warning: 208,
    Level.error: 196,
    Level.wtf: 199,
  };

  static final levelEmojis = {
    Level.verbose: '😒',
    Level.debug: '😑',
    Level.info: '💡😊',
    Level.warning: '😨',
    Level.error: '😱',
    Level.wtf: '👾😭',
  };

  @override
  List<String> log(LogEvent event) {
//    final pen = new AnsiPen()..xterm(255);
    String package;

    try {
      throw "Getting package path";
    } catch(e, stacktrace) {
      try {
        final trace = stacktrace.toString().split('\n')[3];
        package = "(" + trace.split("(")[1];
      } catch(e) {
        final trace = stacktrace.toString().split('\n')[5];
        final source = trace.split(RegExp(r"[ ]{2,}"));
        package = "(${source[0]}) Method '${source[1]}'";
      }
    }

    final level = '(${event.level.toString()})'.padRight(15);
    final source = '$className $package';
    final msg = "${levelEmojis[event.level]} - ${event.message}";

    return [
      "$level $source" +
        "\n" + msg.padLeft(level.length + msg.length + 1)
    ];
  }
}